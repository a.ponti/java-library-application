package test.java;

import static org.junit.Assert.*;

import org.junit.Test;

import main.java.Book;

public class LibraryUnitTests {

	@Test
	public void test() {
		Book b1 = new Book(1, "TestTitle", "TestAuthor", 2000, 200);
		assert(b1.getId() == 1);
		assert(b1.getTitle() == "TestTitle");
		assert(b1.getAuthor() == "TestAuthor");
		assert(b1.getYear() == 2000);
		assert(b1.getPages() == 200);
	}

}
