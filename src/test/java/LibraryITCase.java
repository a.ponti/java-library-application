package integrationtest.java;

import static org.junit.Assert.*;

import main.java.Book;

import main.java.MySQLAccess;

import org.junit.Test;

public class LibraryITCase {

	@Test
	public void test() {
        try {
            Book b1 = new Book(999, "TestTitle1", "TestAuthor1", 999, 999);
            MySQLAccess access = new MySQLAccess();
            access.readDataBase("remotemysql.com/", "R47tRDXpHV", "R47tRDXpHV", "SKCFpQN5ux");
            access.insertBook(b1);
            String title_book = access.getTitleByID(b1.getId());
            assert(title_book.equals(b1.getTitle()));
            access.deleteBook(b1.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

}
