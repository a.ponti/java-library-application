package main.java;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//import java.util.Scanner;

public class MySQLAccess {
    private Connection connect = null;
    private Statement statement = null;
    private ResultSet resultSet = null;
    
    public void readDataBase(String hostname, String dbname, String username, String password) throws Exception {
        try {
            // Load mysql driver
        	Class.forName("com.mysql.jdbc.Driver");

            // Setup the connection with the DB
        	System.out.println("Connection...");
            connect = DriverManager.getConnection("jdbc:mysql://" + hostname + dbname, username, password);
            System.out.println("Connection done!");
            
            statement = connect.createStatement();
            //printLibrary();
            
        } catch (Exception e) {
            throw e;
        }
    }
/*
    public void insertBook() throws SQLException {
    	Scanner in = new Scanner(System.in);
    	String title, author;
    	int id, year, pages;
    	System.out.println("\t### Inserimento nuovo libro ###\n");
    	System.out.print("\tInserisci il titolo: ");
    	title = in.nextLine();
    	System.out.print("\tInserisci l'autore: ");
    	author = in.nextLine();
    	System.out.print("\tInserisci l'anno di pubblicazione: ");
    	year = in.nextInt();
    	System.out.print("\tInserisci il numero di pagine: ");
    	pages = in.nextInt();
    	System.out.print("\tInserisci id: ");
    	id = in.nextInt();
    	String query = "insert into books values(" + id + ", '" + title + "', '" + author + "', " + year + ", " + pages + ")";
    	statement.executeUpdate(query);
    }
*/
    public void insertBook(Book b) throws SQLException {
        String query = "insert into books values(" + b.getId() + ", '" + b.getTitle() + "', '" + b.getAuthor() + "', " + b.getYear() + ", " + b.getPages() + ")";
        statement.executeUpdate(query);
    }
/*
    public void deleteBook() throws SQLException {
    	Scanner in = new Scanner(System.in);
    	int id;
    	System.out.println("\t### Rimozione libro dalla libreria ###\n");
    	System.out.print("\tInserisci id: ");
    	id = Integer.parseInt(in.nextLine());
    	String query = "DELETE FROM books WHERE ID="+ id + "";
    	statement.executeUpdate(query);
    }
*/    
    public void deleteBook(int id) throws SQLException {
    	String query = "DELETE FROM books WHERE ID="+ id + "";
    	statement.executeUpdate(query);
    }
    
    public String getTitleByID(int id) throws SQLException {
        String query = "select * FROM books WHERE ID="+ id + "";
        ResultSet rs = statement.executeQuery(query);
        String title = "";
        while (rs.next()) {
            title = rs.getString("Title");
        }
        return title;
    }
    

    public void printLibrary() throws SQLException {
    	int index = 0;
        resultSet = statement.executeQuery("select * from books");
        System.out.println("\n\t\t\t\t### Libreria ###");
        System.out.print("\n\t\tID\t\t");
        System.out.print("Title\t\t");
        System.out.print("Author\t\t");
        System.out.print("Year\t\t");
        System.out.print("Pages\t\t");
        System.out.println();
        while (resultSet.next()) {
            String id = resultSet.getString("ID");
            String title = resultSet.getString("Title");
            String author = resultSet.getString("Author");
            String year = resultSet.getString("Year");
            String pages = resultSet.getString("Pages");
            System.out.print(index);
            System.out.print("\t\t" + id + "\t\t");
            System.out.print(title + "\t\t");
            System.out.print(author + "\t\t");
            System.out.print(year + "\t\t");
            System.out.print(pages + "\t\t");
            System.out.println();
            index++;
        }
    }

    public void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }
}
