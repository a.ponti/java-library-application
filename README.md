# Assignement 1 Processo e Sviluppo del software

## GRUPPO
* Rizza Simone (#816298)
* Ponti Andrea (#816311)

## APPLICAZIONE
L'applicazione utilizzata per l'assignement 1 è un progetto Java che comunica con un database MySQL.
Essa permette di inserire un libro (Id, Titolo, Autore, Pagine, Anno) all'interno di un database, di eliminare un libro esistente e di visualizzare l'intera libreria.
Per questo progetto si scelto di mostrare solamente una demo del funzionamento dell'applicazione.  
Il database utilizzato dall'applicazione è hostato su remotemysql.com.

## STAGE IMPLEMENTATI
Per lo svilippo della pipeline è stato utilizzato Maven, uno strumento di gestione di progetti software basati su Java.
Di seguito una breve spiegazione degli stage implementati:
- **build:** per la fase di build è stato utilizzato *maven-compiler-plugin* che permette 
  di compilare le classi presenti nel progetto.
- **verify:** per la fase di verify è stato utilizzato *maven-checkstyle-plugin* che genera un 
  report relativo alle problematiche nello stile del codice (si è deciso di non far fallire la pipeline nel caso di warnings).
- **unit-test:** per la fase di unit-test è stato utilizzato *maven-surefire-plugin*. I test 
  sfruttano il framework JUnit, e controllano il corretto funzionamento dei metodi della classe *Book*.
- **integration-test:** per la fase di integration-test è stato utilizzato *maven-surefire-plugin*. 
  I test verificano la corretta integrazione del database MySQL con l'applicazione. 
- **package:** per la fase di package è stato utilizzato *maven-assembly-plugin*. Grazie a 
  questo plugin è stato possibile creare il file *jar* con già integrate tutte le dipendenze.
- **deploy:** per la fase di deploy è stato utilizzato *heroku-maven-plugin* che permette 
  di effetturare il deploy  dell'applicazione su Heroku, una piattaforma cloud per la realizzazione e la distribuzione di applicazioni.

N.B.: *L'applicazione su Heroku crasha al termine dell'esecuzione*.
## FILE: -gitlab-ci.yml

```

image: maven:3.6-jdk-8-slim

variables:
    MAVEN_CLI_OPTS: "--strict-checksums --threads 1C --batch-mode"
    MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"
    
cache:
    key: "$CI_COMMIT_REF_SLUG"
    paths:
        - .m2/repository/
        - target/
            
stages:
    - build
    - verify
    - test
    - integration-test
    - package
    - deploy
        
compile:
    stage: build
    script:
        - mvn compile
            
verify:
    stage: verify
    script:
        - mvn checkstyle:checkstyle

unit-test:
    stage: test
    script:
        - mvn test
        
integration-test:
    stage: integration-test
    script:
        - mvn failsafe:integration-test

library-app:
    stage: package
    cache:
        key: "$CI_COMMIT_REF_SLUG"
        paths:
            - .m2/repository/
        policy: pull
    script:
        - mvn clean compile assembly:single
    artifacts:
        paths:
            - target/*.jar
    when: on_success
        
deploy:
    stage: deploy
    script:
        - mvn heroku:deploy
    when: on_success

```

## REPOSITORY
https://gitlab.com/a.ponti/java-library-application